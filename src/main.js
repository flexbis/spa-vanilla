import './assets/js/routie.js'

import rutas1 from './routes/rutas1.js'
import rutas2 from './routes/rutas2.js'

let objRoutes = {
    ...rutas1, 
    ...rutas2
};

objRoutes = {...objRoutes, '*': function() {
        
        document.querySelector('#app').innerHTML = `
            Página no existe
        `;

    }
}

routie(objRoutes);
