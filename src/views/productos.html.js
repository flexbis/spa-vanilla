export default  () => {

    document.querySelector('#app').innerHTML = `

        <div class="row">
            <div class="col-md-4">
            <div class="card">
                <img src="https://via.placeholder.com/300" class="card-img-top" alt="...">
                <div class="card-body">
                <h5 class="card-title">Artículo 1</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vestibulum quam id justo mollis commodo.</p>
                <a href="#" class="btn btn-primary">Leer más</a>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card">
                <img src="https://via.placeholder.com/300" class="card-img-top" alt="...">
                <div class="card-body">
                <h5 class="card-title">Artículo 2</h5>
                <p class="card-text">Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
                <a href="#" class="btn btn-primary">Leer más</a>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card">
                <img src="https://via.placeholder.com/300" class="card-img-top" alt="...">
                <div class="card-body">
                <h5 class="card-title">Artículo 3</h5>
                <p class="card-text">Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus.</p>
                <a href="#" class="btn btn-primary">Leer más</a>
                </div>
            </div>
            </div>
        </div>

    `;

}
