export default () => {

    document.querySelector('#app').innerHTML = `

        <div class="jumbotron">
            <h1>Bienvenido a mi sitio</h1>
            <p class="lead">¡Descubre todo lo que tenemos para ofrecerte!</p>
            <a class="btn btn-primary btn-lg" href="#" role="button">Explorar</a>
        </div>

        <div class="row">
            <div class="col-md-4">
            <h2>Sección 1</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vestibulum quam id justo mollis commodo.</p>
            <p><a class="btn btn-secondary" href="#" role="button">Detalles &raquo;</a></p>
            </div>
            <div class="col-md-4">
            <h2>Sección 2</h2>
            <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
            <p><a class="btn btn-secondary" href="#" role="button">Detalles &raquo;</a></p>
            </div>
            <div class="col-md-4">
            <h2>Sección 3</h2>
            <p>Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus.</p>
            <p><a class="btn btn-secondary" href="#" role="button">Detalles &raquo;</a></p>
            </div>
        </div>

    `;

}